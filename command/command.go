// command pks implements the cmds action from user, like: set key value, get key, list key start end. etc
package command

type CmdType int
const (
	SimpleKV CmdType = 0
	MultiKV CmdType = 1
	List CmdType = 2
	Set CmdType = 3
	SortedSet CmdType = 4
)

type cmd struct {
	Name string
	Type int
}
type Handler interface {}