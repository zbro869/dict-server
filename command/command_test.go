package command

import (
	"fmt"
	"reflect"
	"testing"
)

func TestCommandType(t *testing.T) {
	fmt.Println(SimpleKV, MultiKV, List, Set, SortedSet)
	fmt.Printf("CmdType: %v, type: %v\n", SimpleKV, reflect.TypeOf(SimpleKV))
}
