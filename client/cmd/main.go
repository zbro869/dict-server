package main

import (
	"flag"
	"fmt"
)

func main() {
	host := flag.String("host", "localhost", "the dict server host name to connect to")
	port := flag.Int("port", 6388, "the port of the server")
	flag.Parse()
	fmt.Printf("---------connecting to host: %s:%d------------\n", *host, *port)
}
