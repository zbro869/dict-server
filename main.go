package main

import (
	"flag"
	"fmt"
	"log"
)

var defaultServerConfig = &serverConfig{
	Host: "localhost",
	Port: uint32(6388),
}

type serverConfig struct {
	Host string `toml:"host"`
	Port uint32 `toml:"port"`
}

// parse toml config file
func parseConfig(path string) *serverConfig {
	return nil
}

func main() {
	configPath := flag.String("config", "config.toml", "dict server config file path")
	flag.Parse()

	var sf *serverConfig
	if *configPath == "" {
		log.Println("config path not set, use default config")
		sf = defaultServerConfig
	}
	fmt.Println("Hello, dict-server: ", sf)
}
