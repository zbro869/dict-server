package cache

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestListContainer_LPush(t *testing.T) {
	testListKey := "test-list"
	cases := []struct {
		desc         string
		keyExists    bool
		values       []string
		newValues    []string
		expectList   []string
		expectLength int
	}{
		{
			desc:         "success insert values to new list",
			keyExists:    false,
			newValues:    []string{"A", "B", "C"},
			expectList:   []string{"C", "B", "A"},
			expectLength: 3,
		},
		{
			desc:         "success insert values to existing list",
			keyExists:    true,
			values:       []string{"A", "B", "C"},
			newValues:    []string{"D", "E", "F"},
			expectList:   []string{"F", "E", "D", "C", "B", "A"},
			expectLength: 6,
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			lc := NewListContainer()
			if c.keyExists {
				_ = lc.LPush(testListKey, c.values...)
				_ = lc.LPush(testListKey, c.newValues...)
			} else {
				_ = lc.LPush(testListKey, c.newValues...)
			}

			result, err := lc.LRange(testListKey, 0, -1)
			assert.Nil(t, err)
			fmt.Println("result: ", result)
			assert.Equal(t, c.expectLength, int(lc.LLen(testListKey)))
			assert.Equal(t, c.expectList, result)
		})
	}
}

func TestListContainer_RPush(t *testing.T) {
	testListKey := "test-list"
	cases := []struct {
		desc       string
		keyExists  bool
		values     []string
		newValues  []string
		expectList []string
	}{
		{
			desc:       "success insert values to new list",
			keyExists:  false,
			newValues:  []string{"A", "B", "C"},
			expectList: []string{"A", "B", "C"},
		},
		{
			desc:       "success insert values to exists key",
			keyExists:  true,
			values:     []string{"A", "B", "C"},
			newValues:  []string{"D", "E", "F"},
			expectList: []string{"A", "B", "C", "D", "E", "F"},
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			lc := NewListContainer()
			if c.keyExists {
				_ = lc.RPush(testListKey, c.values...)
				_ = lc.RPush(testListKey, c.newValues...)
			} else {
				_ = lc.RPush(testListKey, c.newValues...)
			}

			result, err := lc.LRange(testListKey, 0, -1)
			assert.Nil(t, err)
			fmt.Println("result: ", result)
			assert.Equal(t, c.expectList, result)
		})
	}
}

func TestListContainer_LTrim(t *testing.T) {
	testListKey := "test-list"
	testList := []string{"A", "B", "C", "D", "E", "F"}
	expectList := []string{"F", "E", "D", "C", "B", "A"}
	cases := []struct {
		desc         string
		start        int
		end          int
		listDeleted  bool
		expectList   []string
		expectLength int
	}{
		{
			desc:         "not trim [0, -1]",
			start:        0,
			end:          -1,
			listDeleted:  false,
			expectList:   expectList,
			expectLength: 6,
		},
		{
			desc:         "not trim [-6, -1]",
			start:        -6,
			end:          -1,
			listDeleted:  false,
			expectList:   expectList,
			expectLength: 6,
		},
		{
			desc:         "trim [0, -2]",
			start:        0,
			end:          -2,
			listDeleted:  false,
			expectList:   expectList[:5],
			expectLength: 5,
		},
		{
			desc:         "trim - [100, -2]",
			start:        100,
			end:          -2,
			listDeleted:  true,
			expectList:   []string{},
			expectLength: 0,
		},
		{
			desc:         "trim - [0, -6]",
			start:        0,
			end:          -6,
			listDeleted:  false,
			expectList:   []string{"F"},
			expectLength: 1,
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			lc := NewListContainer()
			_ = lc.LPush(testListKey, testList...)

			_ = lc.LTrim(testListKey, c.start, c.end)
			result, err := lc.LRange(testListKey, 0, -1)
			if c.listDeleted {
				assert.Equal(t, ErrEmptyList, err)
				return
			}

			assert.Nil(t, err)
			fmt.Println("result: ", result)
			assert.Equal(t, c.expectList, result)
		})
	}
}

func TestListContainer_LIndex(t *testing.T) {
	testListKey := "test-list"
	testList := []string{"A", "B", "C", "D", "E", "F"}

	cases := []struct {
		desc  string
		index int
		value string
		err   error
	}{
		{
			desc:  "value at index 0",
			index: 0,
			value: "F",
			err:   nil,
		},
		{
			desc:  "value at index -6",
			index: -6,
			value: "F",
			err:   nil,
		},
		{
			desc:  "value at index -1",
			index: -1,
			value: "A",
			err:   nil,
		},
		{
			desc:  "value at index 5",
			index: 5,
			value: "A",
			err:   nil,
		},
		{
			desc:  "value at index 2",
			index: 2,
			value: "D",
			err:   nil,
		},
		{
			desc:  "index out of range(<0)",
			index: -7,
			value: "",
			err:   ErrIndexOutOfRange,
		},
		{
			desc:  "index out of range(>0)",
			index: 6,
			value: "",
			err:   ErrIndexOutOfRange,
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			lc := NewListContainer()
			_ = lc.LPush(testListKey, testList...)
			val, err := lc.LIndex(testListKey, c.index)
			if c.err != nil {
				assert.Equal(t, c.err, err)
			} else {
				assert.Nil(t, err)
				assert.Equal(t, c.value, val)
			}
		})
	}
}

func TestListContainer_LSet(t *testing.T) {
	testListKey := "test-list"
	testList := []string{"A", "B", "C", "D", "E", "F"}
	expectList := []string{"F", "E", "D", "C", "B", "A"}

	copyFn := func() []string {
		expList := make([]string, len(expectList))
		copy(expList, expectList)
		return expList
	}

	cases := []struct {
		desc       string
		index      int
		value      string
		expectList func() []string
		err        error
	}{
		{
			desc:  "set X at 0",
			index: 0,
			value: "X",
			expectList: func() []string {
				expList := copyFn()
				expList[0] = "X"
				return expList
			},
			err: nil,
		},
		{
			desc:  "set X at -6",
			index: -6,
			value: "X",
			expectList: func() []string {
				expList := copyFn()
				expList[0] = "X"
				return expList
			},
			err: nil,
		},
		{
			desc:  "set X at -1",
			index: -1,
			value: "X",
			expectList: func() []string {
				expList := copyFn()
				expList[5] = "X"
				return expList
			},
			err: nil,
		},
		{
			desc:  "set X at 6(out of range)",
			index: 6,
			value: "X",
			expectList: func() []string {
				return nil
			},
			err: ErrIndexOutOfRange,
		},
		{
			desc:  "set X at -7(out of range)",
			index: -7,
			value: "X",
			expectList: func() []string {
				return nil
			},
			err: ErrIndexOutOfRange,
		},
	}
	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			lc := NewListContainer()
			_ = lc.LPush(testListKey, testList...)

			err := lc.LSet(testListKey, c.index, c.value)
			if c.err != nil {
				assert.Equal(t, c.err, err)
			} else {
				lst, _ := lc.LRange(testListKey, 0, -1)
				assert.Nil(t, err)
				assert.Equal(t, c.expectList(), lst)
			}
		})
	}
}
