package cache

type ZSetKey struct {
	Value float64
}
type SortedSet struct {
	dict map[string]float64 // member to score
	list *SkipList
}

func NewSortedSet() *SortedSet {
	return &SortedSet{
		dict: make(map[string]float64),
		list: NewSkipList(),
	}
}

func (s SortedSet) ZAdd(items ...ZSetItem) {
}

func (s SortedSet) ZCard() uint64 {
	return 0
}

func (s SortedSet) ZCount(min, max float64) uint64 {
	return 0
}

func (s SortedSet) ZScore(member string) *ZSetKey {
	score, ok := s.dict[member]
	if !ok {
		return nil
	}

	return &ZSetKey{Value: score}
}

func (s SortedSet) ZRank(member string) uint64 {
	return 0
}

func (s SortedSet) ZRevRank(member string) uint64 {
	panic("implement me")
}

func (s SortedSet) ZRem(members ...string) uint64 {
	deleteCount := uint64(0)
	for _, member := range members {
		if _, ok := s.dict[member]; ok {
			deleteCount++
			delete(s.dict, member)
		}
	}
	return deleteCount
}

func (s SortedSet) ZPopMax(count uint64) []string {
	panic("implement me")
}

func (s SortedSet) ZPopMin(count uint64) []string {
	panic("implement me")
}

func (s SortedSet) ZRange(start, stop int64, withScores bool) []string {
	panic("implement me")
}

func (s SortedSet) ZRangeByScores(min, max float64, withScores bool) []string {
	panic("implement me")
}

func (s SortedSet) ZRevRange(start, stop int64, withScores bool) []string {
	panic("implement me")
}

func (s SortedSet) ZRevRangeByScores(min, max float64, withScores bool) []string {
	panic("implement me")
}

func (s SortedSet) ZRemRangeByRank(start, stop int64) uint64 {
	panic("implement me")
}

func (s SortedSet) ZRemRangeByScore(min, max float64) uint64 {
	panic("implement me")
}
