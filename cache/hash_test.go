package cache

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBatch(t *testing.T) {
	kvs := []int{0, 1, 2, 3, 4, 5}
	for i := 0; i < len(kvs)-1; i += 2 {
		fmt.Printf("kvs[%d]=%d, kvs[%d]=%d\n", i, kvs[i], i+1, kvs[i+1])
	}
}

func convertKvsToMap(kvs []string) map[string]string {
	result := make(map[string]string)
	for i := 0; i < len(kvs)-1; i += 2 {
		result[kvs[i]] = kvs[i+1]
		fmt.Printf("kvs[%s]=%s\n", kvs[i], kvs[i+1])
	}
	return result
}

func TestHashImpl_HSet(t *testing.T) {
	cases := []struct {
		desc         string
		fieldValues  []string
		expectValues map[string]string
		length       uint64
		err          error
	}{
		{
			desc:         "success hset",
			fieldValues:  strings.Split("A 1 B 2 C 3 F 5", " "),
			expectValues: convertKvsToMap(strings.Split("A 1 B 2 C 3 F 5", " ")),
			length:       4,
		},
		{
			desc:        "wrong number of args",
			fieldValues: strings.Split("A 1 B 2 C 3 F 5 D", " "),
			err:         fmt.Errorf(ErrInvalidArgsForCommand, "hset"),
		},
	}

	testHashKey := "test-hash"
	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			h := NewHash()
			fmt.Println("fieldValues: ", c.fieldValues)
			_, err := h.HSet(testHashKey, c.fieldValues...)
			if c.err != nil {
				assert.Equal(t, c.err, err)
				return
			}

			assert.Equal(t, c.length, h.HLen(testHashKey))
			kvs := h.HGetAll(testHashKey)
			fmt.Println("result kvs: ", kvs)
			assert.Equal(t, c.expectValues, convertKvsToMap(kvs))
		})
	}
}
