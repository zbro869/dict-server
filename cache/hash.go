package cache

import (
	"errors"
	"fmt"
	"strconv"
)

const (
	numOfHashKeys            = 256
	ErrInvalidArgsForCommand = "wrong number of arguments for %s command"
)

var (
	ErrHashVlaueIsNotInteger = errors.New("hash value is not integer")
)

type Hash interface {
	HSet(hashName string, kvs ...string) (keyNum uint64, err error)

	// Sets field in the hash stored at key to value,
	// only if field does not yet exist.
	// If key does not exist,
	// a new key holding a hash is created.
	// If field already exists, this operation has no effect.
	HSetNX(hashName string, field, value string) uint8

	HGet(hashName, field string) (value string)
	HGetAll(hashName string) []string
	HKeys(hashName string) []string
	HVals(hashName string) []string
	HLen(hashName string) uint64
	HStrLen(hashName string, field string) uint64
	HDel(hashName string, fields ...string) (keysDeleted uint64)
	HExists(hashName string, field string) bool
	HIncrby(hashName string, field string, increment int64) (int64, error)
	HIncrbyFloat(hashName string, field string, increment float64) (float64, error)
}

type HashImpl struct {
	data map[string]*Dict
}

func NewHash() Hash {
	return &HashImpl{make(map[string]*Dict, numOfHashKeys)}
}

func (h HashImpl) createKeyNotExists(key string) *Dict {
	if _, ok := h.data[key]; !ok {
		h.data[key] = NewDict()
	}
	return h.data[key]
}

func (h HashImpl) HVals(hashName string) []string {
	hash, ok := h.data[hashName]
	if !ok {
		return nil
	}

	var vals []string
	keys := hash.Keys()
	for _, key := range keys {
		val, ok := hash.Get(key)
		if !ok {
			continue
		}

		vals = append(vals, val.(string))
	}
	return vals
}

func (h HashImpl) HStrLen(hashName string, field string) uint64 {
	hash, ok := h.data[hashName]
	if !ok {
		return 0
	}

	val, ok1 := hash.Get(field)
	if !ok1 {
		return 0
	}
	return uint64(len(val.(string)))
}

func (h HashImpl) HSet(hashName string, kvs ...string) (keyNum uint64, err error) {
	d := h.createKeyNotExists(hashName)
	if len(kvs)%2 != 0 {
		return 0, fmt.Errorf(ErrInvalidArgsForCommand, "hset")
	}

	for i := 0; i < len(kvs)-1; i += 2 {
		_ = d.Set(kvs[i], kvs[i+1])
	}
	return uint64(len(kvs)), nil
}

func (h HashImpl) HSetNX(hashName string, field, value string) uint8 {
	d := h.createKeyNotExists(hashName)
	if _, ok := d.Get(field); !ok {
		return 0
	}

	_ = d.Set(field, value)
	return 1
}

func (h HashImpl) HGet(hashName, field string) (value string) {
	hash, ok := h.data[hashName]
	if !ok {
		return ""
	}

	val, ok1 := hash.Get(field)
	if !ok1 {
		return ""
	}

	return val.(string)
}

func (h HashImpl) HGetAll(hashName string) []string {
	hash, ok := h.data[hashName]
	if !ok {
		return nil
	}

	var kvs []string
	keys := hash.Keys()
	for _, key := range keys {
		val, ok := hash.Get(key)
		if !ok {
			continue
		}

		kvs = append(kvs, []string{key, val.(string)}...)
	}
	return kvs
}

func (h HashImpl) HKeys(hashName string) []string {
	hash, ok := h.data[hashName]
	if !ok {
		return nil
	}
	return hash.Keys()
}

func (h HashImpl) HLen(hashName string) uint64 {
	hash, ok := h.data[hashName]
	if !ok {
		return 0
	}
	return hash.Length()
}

func (h HashImpl) HDel(hashName string, fields ...string) (keysDeleted uint64) {
	hash, ok := h.data[hashName]
	if !ok {
		return 0
	}

	for _, field := range fields {
		key := hash.Del(field)
		if key != "" {
			keysDeleted++
		}
	}
	return
}

func (h HashImpl) HExists(hashName string, field string) bool {
	hash, ok := h.data[hashName]
	if !ok {
		return false
	}
	_, exists := hash.Get(field)
	return exists
}

func (h HashImpl) HIncrby(hashName string, field string, increment int64) (int64, error) {
	d := h.createKeyNotExists(hashName)

	val, ok := d.Get(field)
	if !ok {
		_ = d.Set(field, strconv.FormatInt(increment, 10))
		return increment, nil
	}

	intVal, err := strconv.ParseInt(val.(string), 10, 64)
	if err != nil {
		return -1, ErrHashVlaueIsNotInteger
	}

	newVal := intVal + increment
	_ = d.Set(field, strconv.FormatInt(newVal, 10))
	return newVal, nil
}

func (h HashImpl) HIncrbyFloat(hashName string, field string, increment float64) (float64, error) {
	d := h.createKeyNotExists(hashName)
	val, ok := d.Get(field)
	if !ok {
		_ = d.Set(field, strconv.FormatFloat(increment, 'f', -1, 64))
		return increment, nil
	}

	floatVal, err := strconv.ParseFloat(val.(string), 64)
	if err != nil {
		return -1, ErrHashVlaueIsNotInteger
	}

	newVal := floatVal + increment
	_ = d.Set(field, strconv.FormatFloat(newVal, 'f', -1, 64))
	return newVal, nil
}
