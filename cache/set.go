package cache

import (
	"math/rand"
	"time"
)

const numOfSet = 256

type Setter interface {
	SAdd(key string, exp time.Duration, members ...string) uint64
	SCard(key string) uint64
	SDiff(key1, key2 string) []string
	SUnion(key1, key2 string) []string
	SInter(key1, key2 string) []string
	SIsMember(key string, member string) bool
	SMembers(key string) []string
	SPop(key string, num int64) []string
	SRem(key string, member string) bool
}

type Set struct {
	data map[string]*Dict
}

func (s *Set) SCard(key string) uint64 {
	if d, ok := s.data[key]; ok {
		return d.Length()
	}
	return 0
}

func (s *Set) SDiff(key1, key2 string) []string {
	s1, ok := s.data[key1]
	if !ok {
		return []string{}
	}

	s2, ok := s.data[key2]
	if !ok {
		return s1.Keys()
	}

	var diff = []string{}
	s1Members := s1.Keys()
	for _, m1 := range s1Members {
		if _, ok := s2.Get(m1); !ok {
			diff = append(diff, m1)
		}
	}
	return diff
}

func (s *Set) SUnion(key1, key2 string) []string {
	s1, ok1 := s.data[key1]
	s2, ok2 := s.data[key2]
	if ok1 {
		if !ok2 {
			return s1.Keys()
		}
		return union(s1.Keys(), s2.Keys())
	}

	if !ok2 {
		return []string{}
	}
	return s2.Keys()
}

func union(members1, members2 []string) []string {
	result := make(map[string]struct{}, len(members1)+len(members2))
	resultSlice := make([]string, 0, len(members1)+len(members2))

	allMembers := append(members1, members2...)
	for _, m := range allMembers {
		if _, ok := result[m]; ok {
			continue
		}
		resultSlice = append(resultSlice, m)
		result[m] = struct{}{}
	}
	return resultSlice
}

func (s *Set) SInter(key1, key2 string) []string {
	s1, ok1 := s.data[key1]
	s2, ok2 := s.data[key2]
	if !ok1 || !ok2 {
		return []string{}
	}

	return intersection(s1.Keys(), s2)
}

func intersection(s1Members []string, s2 *Dict) (result []string) {
	for _, m := range s1Members {
		if _, ok := s2.Get(m); ok {
			result = append(result, m)
		}
	}
	return
}

func (s *Set) SIsMember(key string, member string) bool {
	if v, ok := s.data[key]; ok {
		_, mok := v.Get(member)
		return mok
	}
	return false
}

func (s *Set) SMembers(key string) []string {
	if v, ok := s.data[key]; ok {
		return v.Keys()
	}
	return []string{}
}

func (s *Set) SPop(key string, num int64) []string {
	if num == 0 {
		return nil
	}

	v, ok := s.data[key]
	if !ok {
		return nil
	}

	members := v.Keys()
	if len(members) == 0 {
		return []string{}
	}

	var result []string
	shuffle(members)

	var mIdx int
	for num >= 1 && mIdx < len(members) {
		k := members[mIdx]
		v.Del(k)
		result = append(result, k)
		mIdx++
		num--
	}
	return result
}

func shuffle(slice []string) {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	for len(slice) > 0 {
		n := len(slice)
		randIndex := r.Intn(n)
		slice[n-1], slice[randIndex] = slice[randIndex], slice[n-1]
		slice = slice[:n-1]
	}
}

func (s *Set) SRem(key string, member string) bool {
	v, ok := s.data[key]
	if !ok {
		return false
	}

	_, hasKey := v.Get(member)
	v.Del(member)
	return hasKey
}

func NewSetter() Setter {
	s := &Set{
		data: make(map[string]*Dict, numOfSet),
	}
	return s
}

func (s *Set) SAdd(key string, exp time.Duration, members ...string) uint64 {
	if v, ok := s.data[key]; ok {
		for _, m := range members {
			_ = v.SetWithExpiration(m, nil, time.Now().Add(exp))
		}
		return v.Length()
	}

	d := NewDict()
	for _, m := range members {
		_ = d.SetWithExpiration(m, nil, time.Now().Add(exp))
	}
	s.data[key] = d
	return d.Length()
}
