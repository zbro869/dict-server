package cache

import (
	"strings"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDict_Get(t *testing.T) {
	keys := strings.Split("A E D F H J L", " ")
	cases := []struct {
		key    string
		exists bool
	}{
		{
			key:    "A",
			exists: true,
		},
		{
			key:    "AE",
			exists: false,
		},
		{
			key:    "L",
			exists: true,
		},
	}

	d := NewDict()
	for _, k := range keys {
		_ = d.Set(k, nil)
	}

	for _, c := range cases {
		_, ok := d.Get(c.key)
		assert.Equal(t, c.exists, ok)
	}
}

func TestDict_Set(t *testing.T) {

}

func TestDict_SetWithExpiration(t *testing.T) {

}

func TestDict_Length(t *testing.T) {
	var wg sync.WaitGroup
	d := NewDict()
	keysArr := [][]string{
		{"A", "B", "C"},
		{"D", "E", "F"},
		{"H", "J", "K"},
		{"A", "E", "F"},
	}

	for _, keys := range keysArr {
		for _, key := range keys {
			wg.Add(1)
			go func(k string) {
				_ = d.Set(k, nil)
				wg.Done()
			}(key)
		}
	}
	wg.Wait()

	assert.Equal(t, uint64(9), d.Length())
}
