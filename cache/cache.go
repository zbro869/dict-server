/*
cache pkg implements all kinds of data structure store and retrieve functions.
*/
package cache

import (
	"sync"
	"sync/atomic"
	"time"

	"github.com/cespare/xxhash"
)

const (
	numShards     uint64 = 256
	lockStoreSize        = 8
)

// DictI dict interface
type DictI interface {
	Get(key string) (value interface{}, ok bool)
	Set(key string, value interface{}) error
	Del(key string)
	SetWithExpiration(key string, value interface{}, exp time.Time) error
	Length() uint64
	Keys() []string
}

type item struct {
	key        string
	value      interface{}
	expiration time.Time
}

type lockStore struct {
	dict *Dict
	sync.RWMutex
	data map[string]item
}

func newLockStore(dict *Dict) *lockStore {
	return &lockStore{
		dict: dict,
		data: make(map[string]item, lockStoreSize),
	}
}

func (ls *lockStore) Get(key string) (item, bool) {
	ls.RLock()
	defer ls.RUnlock()
	result, ok := ls.data[key]
	return result, ok
}

func (ls *lockStore) Set(i *item) {
	if i == nil {
		return
	}

	ls.Lock()
	if _, ok := ls.data[i.key]; !ok {
		ls.dict.IncreaseLength(uint64(1))
	}
	ls.data[i.key] = *i
	ls.Unlock()
}

func (ls *lockStore) Keys() (keys []string) {
	ls.Lock()
	defer ls.Unlock()
	for k := range ls.data {
		keys = append(keys, k)
	}
	return keys
}

func (ls *lockStore) Del(key string) string {
	ls.Lock()
	defer ls.Unlock()
	if _, ok := ls.data[key]; ok {
		ls.dict.DecreaseLength(1)
		delete(ls.data, key)
		return key
	}
	return ""
}

type Dict struct {
	shards []*lockStore
	length uint64
}

func NewDict() *Dict {
	d := &Dict{
		shards: make([]*lockStore, numShards),
		length: 0,
	}
	for i := range d.shards {
		d.shards[i] = newLockStore(d)
	}
	return d
}

func (d *Dict) Get(key string) (interface{}, bool) {
	sd := d.shards[keyToHash(key)%numShards]
	v, ok := sd.Get(key)
	if !ok {
		return nil, false
	}
	return v.value, true
}

func (d *Dict) Set(key string, value interface{}) error {
	sd := d.shards[keyToHash(key)%numShards]
	sd.Set(&item{
		key:        key,
		value:      value,
		expiration: time.Time{},
	})
	return nil
}

func (d *Dict) SetWithExpiration(key string, value interface{}, exp time.Time) error {
	sd := d.shards[keyToHash(key)%numShards]
	sd.Set(&item{
		key:        key,
		value:      value,
		expiration: exp,
	})
	return nil
}

func (d *Dict) Del(key string) string {
	sd := d.shards[keyToHash(key)%numShards]
	return sd.Del(key)
}

func (d *Dict) IncreaseLength(delta uint64) {
	atomic.AddUint64(&d.length, delta)
}

func (d *Dict) DecreaseLength(delta uint64) {
	atomic.AddUint64(&d.length, -delta)
}

func (d *Dict) Length() uint64 {
	return atomic.LoadUint64(&d.length)
}

func (d *Dict) Keys() (keys []string) {
	var mu sync.Mutex
	mu.Lock()
	for _, ls := range d.shards {
		keys = append(keys, ls.Keys()...)
	}
	mu.Unlock()
	return
}

func keyToHash(key string) uint64 {
	return xxhash.Sum64String(key)
}
