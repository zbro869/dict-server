package cache

type ZSetItem struct {
	Score ZSetKey
	Value string
}

type SortedSetter interface {
	ZAdd(key string, items ...ZSetItem)
	ZCard(key string) uint64
	ZCount(key string, min, max float64) uint64
	ZScore(key string, member string) float64
	ZRank(key string, member string) uint64
	ZRevRank(key string, member string) uint64
	ZRem(key string, members ...string) uint64
	ZPopMax(key string, count uint64) []string
	ZPopMin(key string, count uint64) []string
	ZRange(key string, start, stop int64, withScores bool) []string
	ZRangeByScores(key string, min, max float64, withScores bool) []string
	ZRevRange(key string, start, stop int64, withScores bool) []string
	ZRevRangeByScores(key string, min, max float64, withScores bool) []string
	ZRemRangeByRank(key string, start, stop int64) uint64
	ZRemRangeByScore(key string, min, max float64) uint64
}

type SortedSetContainer struct {
	data *Dict
}

func (s SortedSetContainer) ZAdd(key string, items ...ZSetItem) {
	panic("implement me")
}

func (s SortedSetContainer) ZCard(key string) uint64 {
	panic("implement me")
}

func (s SortedSetContainer) ZCount(key string, min, max float64) uint64 {
	panic("implement me")
}

func (s SortedSetContainer) ZScore(key string, member string) float64 {
	panic("implement me")
}

func (s SortedSetContainer) ZRank(key string, member string) uint64 {
	panic("implement me")
}

func (s SortedSetContainer) ZRevRank(key string, member string) uint64 {
	panic("implement me")
}

func (s SortedSetContainer) ZRem(key string, members ...string) uint64 {
	panic("implement me")
}

func (s SortedSetContainer) ZPopMax(key string, count uint64) []string {
	panic("implement me")
}

func (s SortedSetContainer) ZPopMin(key string, count uint64) []string {
	panic("implement me")
}

func (s SortedSetContainer) ZRange(key string, start, stop int64, withScores bool) []string {
	panic("implement me")
}

func (s SortedSetContainer) ZRangeByScores(key string, min, max float64, withScores bool) []string {
	panic("implement me")
}

func (s SortedSetContainer) ZRevRange(key string, start, stop int64, withScores bool) []string {
	panic("implement me")
}

func (s SortedSetContainer) ZRevRangeByScores(key string, min, max float64, withScores bool) []string {
	panic("implement me")
}

func (s SortedSetContainer) ZRemRangeByRank(key string, start, stop int64) uint64 {
	panic("implement me")
}

func (s SortedSetContainer) ZRemRangeByScore(key string, min, max float64) uint64 {
	panic("implement me")
}

func NewZSet() SortedSetter {
	return &SortedSetContainer{
		data: NewDict(),
	}
}
