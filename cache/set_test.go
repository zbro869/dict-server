package cache

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestSet_SAdd(t *testing.T) {
	cases := []struct {
		members       []string
		expectMembers map[string]struct{}
	}{
		{
			members:       []string{"test-member"},
			expectMembers: map[string]struct{}{"test-member": {}},
		},
		{
			members: []string{"Time", "Forever", "Live", "TM"},
			expectMembers: map[string]struct{}{
				"Time":    {},
				"Forever": {},
				"Live":    {},
				"TM":      {},
			},
		},
	}

	testSetKey := "test-set"
	for _, c := range cases {
		s := NewSetter()
		s.SAdd(testSetKey, 1*time.Second, c.members...)

		members := s.SMembers(testSetKey)
		var ms = make(map[string]struct{})
		for _, m := range members {
			ms[m] = struct{}{}
		}
		fmt.Println("members: ", members)
		assert.Equal(t, c.expectMembers, ms)
	}
}

func TestSet_SIsMember(t *testing.T) {
	cases := []struct {
		desc     string
		members  []string
		member   string
		isMember bool
	}{
		{
			desc:     "member exists",
			members:  []string{"Time", "Forever", "Live", "TM"},
			member:   "Forever",
			isMember: true,
		},
		{
			desc:     "member not exists",
			members:  []string{"A", "B", "D", "E"},
			member:   "G",
			isMember: false,
		},
	}

	testSetKey := "test-set"
	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			s := NewSetter()
			s.SAdd(testSetKey, 1*time.Second, c.members...)

			result := s.SIsMember(testSetKey, c.member)
			assert.Equal(t, c.isMember, result)
		})
	}
}

func TestSet_SRem(t *testing.T) {
	cases := []struct {
		desc     string
		members  []string
		member   string
		isMember bool
	}{
		{
			desc:     "del member exists",
			members:  []string{"Time", "Forever", "Live", "TM"},
			member:   "Forever",
			isMember: true,
		},
		{
			desc:     "del member not exists",
			members:  []string{"A", "B", "D", "E"},
			member:   "G",
			isMember: false,
		},
	}

	testSetKey := "test-set"
	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			s := NewSetter()
			s.SAdd(testSetKey, 1*time.Second, c.members...)

			result := s.SRem(testSetKey, c.member)
			assert.Equal(t, c.isMember, result)
		})
	}
}

func TestSet_SPop(t *testing.T) {
	cases := []struct {
		desc                string
		members             []string
		popNum              int
		expectMembersLength uint64
	}{
		{
			desc:                "pop 1 member",
			members:             []string{"test-member"},
			popNum:              1,
			expectMembersLength: 0,
		},
		{
			desc:                "pop 2 member",
			members:             []string{"Time", "Forever", "Live", "TM"},
			popNum:              2,
			expectMembersLength: 2,
		},
		{
			desc:                "pop all member",
			members:             []string{"Time", "Forever", "Live", "TM"},
			popNum:              4,
			expectMembersLength: 0,
		},
	}

	testSetKey := "test-set"
	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			s := NewSetter()
			s.SAdd(testSetKey, 1*time.Second, c.members...)

			members := s.SPop(testSetKey, int64(c.popNum))
			assert.Equal(t, c.popNum, len(members))
			assert.Equal(t, c.expectMembersLength, s.SCard(testSetKey))
		})
	}
}

func TestSet_SDiff(t *testing.T) {
	cases := []struct {
		desc        string
		s1Members   []string
		s2Members   []string
		s1NotExists bool
		s2NotExists bool
		diffMembers []string
	}{
		{
			desc:        "have diff",
			s1Members:   strings.Split("A B C", " "),
			s2Members:   strings.Split("B C D", " "),
			s1NotExists: false,
			s2NotExists: false,
			diffMembers: []string{"A"},
		},
		{
			desc:        "have no diff",
			s1NotExists: false,
			s2NotExists: false,
			s1Members:   strings.Split("A B C", " "),
			s2Members:   strings.Split("A B C", " "),
			diffMembers: []string{},
		},
		{
			desc:        "s1 not exists",
			s1NotExists: true,
			s2Members:   strings.Split("D E F", " "),
			diffMembers: []string{},
		},
		{
			desc:        "s2 not exists",
			s2NotExists: true,
			s1Members:   strings.Split("A B C", " "),
			diffMembers: []string{"A", "B", "C"},
		},
	}
	testSet1Key, testSet2Key := "test-set1", "test-set2"
	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			setter := NewSetter()
			if !c.s1NotExists {
				setter.SAdd(testSet1Key, 1*time.Second, c.s1Members...)
			}

			if !c.s2NotExists {
				setter.SAdd(testSet2Key, 1*time.Second, c.s2Members...)
			}

			diff := setter.SDiff(testSet1Key, testSet2Key)
			assert.Equal(t, len(c.diffMembers), len(diff))
		})
	}
}

func TestSet_SUnion(t *testing.T) {
	cases := []struct {
		desc         string
		s1Members    []string
		s2Members    []string
		s1NotExists  bool
		s2NotExists  bool
		unionMembers []string
	}{
		{
			desc:         "have 4 union members",
			s1Members:    strings.Split("A B C", " "),
			s2Members:    strings.Split("B C D", " "),
			s1NotExists:  false,
			s2NotExists:  false,
			unionMembers: strings.Split("A B C D", " "),
		},
		{
			desc:         "have same members",
			s1NotExists:  false,
			s2NotExists:  false,
			s1Members:    strings.Split("A B C", " "),
			s2Members:    strings.Split("A B C", " "),
			unionMembers: strings.Split("A B C", " "),
		},
		{
			desc:         "s1 not exists",
			s1NotExists:  true,
			s2Members:    strings.Split("D E F", " "),
			unionMembers: strings.Split("D E F", " "),
		},
		{
			desc:         "s2 not exists",
			s2NotExists:  true,
			s1Members:    strings.Split("A B C", " "),
			unionMembers: []string{"A", "B", "C"},
		},
	}
	testSet1Key, testSet2Key := "test-set1", "test-set2"
	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			setter := NewSetter()
			if !c.s1NotExists {
				setter.SAdd(testSet1Key, 1*time.Second, c.s1Members...)
			}

			if !c.s2NotExists {
				setter.SAdd(testSet2Key, 1*time.Second, c.s2Members...)
			}

			diff := setter.SUnion(testSet1Key, testSet2Key)
			assert.Equal(t, membersToMap(c.unionMembers), membersToMap(diff))
		})
	}
}

func TestSet_SInter(t *testing.T) {
	cases := []struct {
		desc         string
		s1Members    []string
		s2Members    []string
		s1NotExists  bool
		s2NotExists  bool
		interMembers []string
	}{
		{
			desc:         "have 2 inter members",
			s1Members:    strings.Split("A B C", " "),
			s2Members:    strings.Split("B C D", " "),
			s1NotExists:  false,
			s2NotExists:  false,
			interMembers: strings.Split("B C", " "),
		},
		{
			desc:         "have same members",
			s1NotExists:  false,
			s2NotExists:  false,
			s1Members:    strings.Split("A B C", " "),
			s2Members:    strings.Split("A B C", " "),
			interMembers: strings.Split("A B C", " "),
		},
		{
			desc:         "s1 not exists",
			s1NotExists:  true,
			s2Members:    strings.Split("D E F", " "),
			interMembers: []string{},
		},
		{
			desc:         "s2 not exists",
			s2NotExists:  true,
			s1Members:    strings.Split("A B C", " "),
			interMembers: []string{},
		},
	}
	testSet1Key, testSet2Key := "test-set1", "test-set2"
	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			setter := NewSetter()
			if !c.s1NotExists {
				setter.SAdd(testSet1Key, 1*time.Second, c.s1Members...)
			}

			if !c.s2NotExists {
				setter.SAdd(testSet2Key, 1*time.Second, c.s2Members...)
			}

			diff := setter.SInter(testSet1Key, testSet2Key)
			assert.Equal(t, membersToMap(c.interMembers), membersToMap(diff))
		})
	}
}

func membersToMap(members []string) map[string]struct{} {
	m := make(map[string]struct{})
	for _, mb := range members {
		m[mb] = struct{}{}
	}
	return m
}
