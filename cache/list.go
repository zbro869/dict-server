package cache

import (
	"container/list"
	"errors"
	"math"
)

const (
	// TODO make it a config option
	defaultNumOfList = 128
)

var (
	ErrEmptyList         = errors.New("empty list")
	ErrNoSuchKey         = errors.New("no such key")
	ErrInvalidArgs       = errors.New("invalid args")
	ErrNilValue          = errors.New("<nil>")
	ErrIndexOutOfRange   = errors.New("index out of range")
	ErrIndexRangeInvalid = errors.New("index range invalid")
)

type Lister interface {
	LPush(key string, values ...string) error
	RPush(key string, values ...string) error
	LRange(key string, start, end int) ([]string, error)
	LLen(key string) uint64
	LPop(key string) string
	LIndex(key string, index int) (string, error)
	LSet(key string, index int, value string) error
	LTrim(key string, start, end int) error
}

// ListContainer manage keys of list of strings
type ListContainer struct {
	container map[string]List
}

func NewListContainer() *ListContainer {
	return &ListContainer{
		container: make(map[string]List, defaultNumOfList),
	}
}

func (lc *ListContainer) LPush(key string, values ...string) error {
	if len(values) <= 0 {
		return ErrInvalidArgs
	}

	lst, ok := lc.container[key]
	if !ok {
		newList := List{data: list.New()}
		for _, v := range values {
			newList.InsertToFront(v)
		}

		lc.container[key] = newList
		return nil
	}

	for _, v := range values {
		lst.InsertToFront(v)
	}
	return nil
}

func (lc *ListContainer) RPush(key string, values ...string) error {
	if len(values) <= 0 {
		return ErrInvalidArgs
	}

	lst, ok := lc.container[key]
	if !ok {
		newList := List{data: list.New()}
		for _, v := range values {
			newList.AppendToTail(v)
		}

		lc.container[key] = newList
		return nil
	}

	for _, v := range values {
		lst.AppendToTail(v)
	}
	return nil
}

func (lc *ListContainer) LRange(key string, start, end int) ([]string, error) {
	lst, ok := lc.container[key]
	if !ok {
		return []string{}, ErrEmptyList
	}
	return lst.Range(start, end)
}

func (lc *ListContainer) LLen(key string) uint64 {
	lst, ok := lc.container[key]
	if !ok {
		return 0
	}
	return lst.Len()
}

func (lc *ListContainer) LPop(key string) string {
	lst, ok := lc.container[key]
	if !ok {
		return ""
	}
	return lst.DeleteFront()
}

func (lc *ListContainer) LIndex(key string, index int) (string, error) {
	lst, ok := lc.container[key]
	if !ok {
		return "", ErrEmptyList
	}
	return lst.Find(index)
}

// https://redis.io/commands/lset
// if no such key, return error
func (lc *ListContainer) LSet(key string, index int, value string) error {
	lst, ok := lc.container[key]
	if !ok {
		return ErrNoSuchKey
	}

	return lst.Set(index, value)
}

// https://redis.io/commands/ltrim
func (lc *ListContainer) LTrim(key string, start, end int) error {
	lst, ok := lc.container[key]
	if !ok {
		return ErrNoSuchKey
	}

	deleteList, newList := lst.Slice(start, end)
	if deleteList {
		delete(lc.container, key)
		return nil
	}

	lc.container[key] = *newList
	return nil
}

type List struct {
	data *list.List
}

func (l *List) InsertToFront(data string) {
	l.data.PushFront(data)
}

func (l *List) AppendToTail(data string) {
	l.data.PushBack(data)
}

func (l *List) DeleteFront() string {
	var result string
	e := l.data.Front()
	if e != nil {
		result = e.Value.(string)
	}

	l.data.Remove(e)
	return result
}

func (l *List) Range(start, end int) ([]string, error) {
	left, right, err := l.getRightRangeIndex(start, end)
	if err != nil {
		return nil, err
	}

	var result []string
	var idx int
	for e := l.data.Front(); e != nil; e = e.Next() {
		if idx >= left && idx <= right {
			result = append(result, e.Value.(string))
		}
		idx++
	}
	return result, nil
}

func (l *List) getRightRangeIndex(left, right int) (int, int, error) {
	var lidx, ridx int
	length := float64(l.Len())
	absLeft := math.Abs(float64(left))
	if left > 0 && absLeft > length {
		return 0, 0, ErrIndexRangeInvalid
	}

	if left < 0 && absLeft > length {
		lidx = 0
	}

	if left < 0 && absLeft <= length {
		lidx = int(length - absLeft)
	}

	absRight := math.Abs(float64(right))
	if absRight > length {
		ridx = int(length - 1)
	}

	if right < 0 && absRight > length {
		return 0, 0, ErrIndexRangeInvalid
	}

	if right < 0 && absRight <= length {
		ridx = int(length - absRight)
	}

	if lidx > ridx {
		return 0, 0, ErrIndexRangeInvalid
	}

	return lidx, ridx, nil
}

func (l *List) Slice(left, right int) (delete bool, newList *List) {
	length := int(l.Len())

	if left < 0 {
		absLeft := 0 - left
		if absLeft >= length {
			left = 0
		} else {
			left = length - absLeft
		}
	}

	if right < 0 {
		absRight := 0 - right
		if absRight > length {
			return true, nil
		}

		if absRight <= length {
			right = length + right
		}
	}

	if left > right || left > length {
		return true, nil
	}

	if left == 0 && right == length-1 {
		return false, l
	}

	newList = &List{
		data: list.New(),
	}
	var idx int
	for e := l.data.Front(); e != nil; e = e.Next() {
		if idx >= left && idx <= right {
			idx++
			newList.AppendToTail(e.Value.(string))
			continue
		}
		idx++
	}
	return false, newList
}

func (l *List) Len() uint64 {
	return uint64(l.data.Len())
}

func (l *List) Find(index int) (string, error) {
	if !l.isIndexRangeValid(index) {
		return "", ErrIndexOutOfRange
	}

	if index < 0 {
		var idx = -1
		for e := l.data.Back(); e != nil; e = e.Prev() {
			if idx == index {
				return e.Value.(string), nil
			}
			idx--
		}
		return "", ErrNilValue
	}

	var idx int
	for e := l.data.Front(); e != nil; e = e.Next() {
		if idx == index {
			return e.Value.(string), nil
		}
		idx++
	}
	return "", ErrNilValue
}

func (l *List) Set(index int, value string) error {
	if !l.isIndexRangeValid(index) {
		return ErrIndexOutOfRange
	}

	if index < 0 {
		var idx = -1
		for e := l.data.Back(); e != nil; e = e.Prev() {
			if idx == index {
				e.Value = value
				break
			}
			idx--
		}
		return nil
	}

	var idx int
	for e := l.data.Front(); e != nil; e = e.Next() {
		if idx == index {
			e.Value = value
			break
		}
		idx++
	}
	return nil
}

func (l *List) isIndexRangeValid(idx int) bool {
	if idx >= 0 {
		return idx < int(l.Len())
	}

	return idx <= -1 && idx >= -int(l.Len())
}
