package cache

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSkipListNodeEqual(t *testing.T) {
	node1 := &skipListNode{
		key:   0,
		value: "1",
	}
	node2 := &skipListNode{
		key:   0,
		value: "1",
	}
	assert.Equal(t, node1, node2)

	node1 = &skipListNode{
		key:   100,
		value: "A",
	}
	node2 = &skipListNode{
		key:   0,
		value: "1",
	}
	assert.NotEqual(t, node1, node2)

	node1 = nil
	node2 = &skipListNode{
		key:   0,
		value: "1",
	}
	assert.NotEqual(t, node1, node2)

	node1 = &skipListNode{
		key:   100,
		value: "A",
	}
	node2 = nil
	assert.NotEqual(t, node1, node2)
}

func TestSkipList_Set(t *testing.T) {
	cases := []struct {
		desc         string
		kvs          []ZSetItem
		expectLength int
	}{
		{
			desc: "success insert items",
			kvs: []ZSetItem{
				{Score: ZSetKey{1}, Value: "A"},
				{Score: ZSetKey{100}, Value: "B"},
				{Score: ZSetKey{88}, Value: "C"},
			},
			expectLength: 3,
		},
		{
			desc: "have duplicate keys",
			kvs: []ZSetItem{
				{Score: ZSetKey{1}, Value: "A"},
				{Score: ZSetKey{100}, Value: "B"},
				{Score: ZSetKey{88}, Value: "C"},
				{Score: ZSetKey{100}, Value: "D"},
			},
			expectLength: 4,
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			l := NewSkipList()
			for _, item := range c.kvs {
				l.Set(item.Score.Value, item.Value)
			}
			assert.Equal(t, uint64(c.expectLength), l.Length())
		})
	}
}

func TestSkipList_Get(t *testing.T) {
	kvs := []ZSetItem{
		{Score: ZSetKey{1}, Value: "A"},
		{Score: ZSetKey{100}, Value: "B"},
		{Score: ZSetKey{88}, Value: "C"},
		{Score: ZSetKey{66}, Value: "D"},
	}

	cases := []struct {
		desc        string
		key         float64
		hasValue    bool
		expectValue string
	}{
		{
			desc:        "get 100",
			key:         100,
			hasValue:    true,
			expectValue: "B",
		},
		{
			desc:        "get 88",
			key:         88,
			hasValue:    true,
			expectValue: "C",
		},
		{
			desc:        "get 66",
			key:         66,
			hasValue:    true,
			expectValue: "D",
		},
		{
			desc:     "get 1000",
			key:      1000,
			hasValue: false,
		},
	}

	l := NewSkipList()
	for _, item := range kvs {
		l.Set(item.Score.Value, item.Value)
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			value, ok := l.Get(c.key)
			if !c.hasValue {
				assert.False(t, ok)
				return
			}
			assert.Equal(t, c.expectValue, value)
		})
	}
}
