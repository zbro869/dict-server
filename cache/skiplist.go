package cache

import (
	"fmt"
	"math"
	"math/rand"
	"strings"
	"time"
)

const (
	p               = 0.25
	defaultMaxLevel = 32
)

type skipListNode struct {
	key      float64
	value    string
	backward *skipListNode
	levels   []*skipListLevel
}

type skipListLevel struct {
	forward *skipListNode
	span    uint // span用于计算元素排名(rank
}

type SkipList struct {
	head   *skipListNode
	tail   *skipListNode
	length uint64
	level  int // current level
}

func newSkipListNode(key float64, value string, len, cap int) *skipListNode {
	return &skipListNode{
		key:      key,
		value:    value,
		backward: nil,
		levels:   make([]*skipListLevel, len, cap),
	}
}

func NewSkipList() *SkipList {
	head := newSkipListNode(float64(math.MinInt64), "", 1, defaultMaxLevel+1)
	l := &SkipList{
		head:   head,
		tail:   nil,
		length: 0,
		level:  0,
	}
	for i := 0; i < len(l.head.levels); i++ {
		l.head.levels[i] = &skipListLevel{forward: nil}
	}
	rand.Seed(time.Now().UnixNano())
	return l
}

func (sl *SkipList) Length() uint64 {
	return sl.length
}

func (sl *SkipList) getLevel() int {
	return len(sl.head.levels) - 1
}

func (sl *SkipList) randomLevel() (l int) {
	for l = 0; l < defaultMaxLevel && rand.Float64() < p; l++ {
	}
	return l
}

func lessThan(forward *skipListNode, key float64, value string) bool {
	return forward.key < key || (forward.key == key && strings.Compare(forward.value, value) < 0)
}

// Steps:
// generate update and rank array
// create a new node with random level
// insert new node according to update and rank info
// update other necessary infos, such as span, backward pointer, length.
func (sl *SkipList) Set(key float64, value string) {
	currentLevel := sl.getLevel()
	update := make([]*skipListNode, currentLevel+1, defaultMaxLevel+1)
	rank := make([]uint, defaultMaxLevel+1)

	currentNode := sl.head
	for i := currentLevel; i >= 0; i-- {
		if i == currentLevel {
			rank[i] = 0
		} else {
			rank[i] = rank[i+1]
		}

		curNodeLevel := currentNode.levels[i] // down to next level
		if curNodeLevel != nil && curNodeLevel.forward != nil && lessThan(curNodeLevel.forward, key, value) {
			rank[i] += curNodeLevel.span
			currentNode = curNodeLevel.forward // forward to next node
		}
		update[i] = currentNode
	}

	if currentNode != nil && currentNode.key == key {
		currentNode.value = value
		return
	}

	randomLevel := sl.randomLevel()
	fmt.Println("[Set] key: ", key, ", randomLevel: ", randomLevel)
	if randomLevel > currentLevel {
		for i := currentLevel + 1; i <= randomLevel; i++ {
			rank[i] = 0
			update = append(update, sl.head)
			sl.head.levels = append(sl.head.levels, &skipListLevel{forward: nil})
			update[i].levels[i].span = uint(sl.length)
		}
		sl.level = randomLevel
	}

	newNode := newSkipListNode(key, value, randomLevel+1, defaultMaxLevel+1)
	for idx := 0; idx < len(newNode.levels); idx++ {
		newNode.levels[idx] = &skipListLevel{
			forward: nil,
			span:    0,
		}
	}

	previousNode := update[0]
	if previousNode != nil {
		newNode.backward = previousNode
	}

	for i := 0; i <= randomLevel; i++ {
		newNode.levels[i].forward = update[i].levels[i].forward
		update[i].levels[i].forward = newNode

		newNode.levels[i].span = update[i].levels[i].span - (rank[0] - rank[i])
		update[i].levels[i].span = (rank[0] - rank[i]) + 1
	}
	for i := randomLevel; i <= sl.getLevel(); i++ {
		update[i].levels[i].span++
	}

	if newNode.levels[0] != nil {
		fwrd := newNode.levels[0].forward
		if fwrd != nil && fwrd.backward != newNode {
			fwrd.backward = newNode
		}
	}

	if sl.tail == nil || sl.tail.key < key {
		sl.tail = newNode
	}
	sl.length++
}

func (sl *SkipList) Get(key float64) (value string, ok bool) {
	currentLevel := sl.getLevel()
	currentNode := sl.head

	for i := currentLevel; i >= 0; i-- {
		level := currentNode.levels[i]
		for level != nil && level.forward != nil && lessThan(level.forward, key, "") {
			currentNode = level.forward
			level = currentNode.levels[i]
		}
	}

	if currentNode == nil || len(currentNode.levels) == 0 || currentNode.levels[0].forward == nil {
		return "", false
	}

	if currentNode.levels[0].forward.key != key {
		return "", false
	}

	return currentNode.levels[0].forward.value, true
}

func (sl *SkipList) Delete(key float64, value string) (ok bool) {
	currentLevel := sl.getLevel()
	update := make([]*skipListNode, currentLevel+1, defaultMaxLevel+1)

	currentNode := sl.head
	for i := currentLevel; i >= 0; i-- {
		level := currentNode.levels[i]
		if level != nil && level.forward != nil && lessThan(level.forward, key, "") {
			currentNode = level.forward
		}
		update[i] = currentNode
	}

	// after compare, currentNode key >= key
	node := currentNode.levels[0].forward
	if node != nil && node.key == key && strings.Compare(node.value, value) == 0 {
		sl.deleteNode(node, update)
		return true
	}

	return false
}

func skipNodeEqual(node1, node2 *skipListNode) bool {
	if node1 != nil {
		if node2 == nil {
			return false
		}

		return node1.key == node2.key && node1.value == node2.value
	}

	return node2 == nil
}

func (sl *SkipList) deleteNode(node *skipListNode, update []*skipListNode) {
	curLevel := sl.getLevel()
	for i := 0; i <= curLevel; i++ {
		if skipNodeEqual(update[i].levels[i].forward, node) {
			update[i].levels[i].forward = node.levels[i].forward
		}
	}

	if node.levels[0].forward != nil {
		node.levels[0].forward.backward = node.backward
	} else {
		sl.tail = node.backward
	}

	for curLevel > 0 && sl.head.levels[curLevel].forward == nil {
		curLevel--
	}

	sl.level = curLevel
	sl.head.levels = sl.head.levels[:curLevel+1]
	sl.length--
}
