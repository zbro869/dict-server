## Skip List

```go
package cache

const (
	defautMaxLevel = 32
)

type skipListNode struct {
	key float64
	value string
	prev *skipListNode
	levels []*skipListLevel
}

type skipListLevel struct {
	next []*skipListNode
	span uint // span用于计算元素排名(rank
}

type SkipList struct {
	head *skipListNode
	tail *skipListNode
	length uint64
	level uint
}

func newSkipListNode(key float64, value string) *skipListNode {
	return &skipListNode{
		key:    key,
		value:  value,
		prev:   nil,
		levels: nil,
	}
}

func NewSkipList() *SkipList {
	return &SkipList{
		head:   nil,
		tail:   nil,
		length: 0,
		level:  0,
	}
}

```

* https://www.memsql.com/blog/what-is-skiplist-why-skiplist-index-for-memsql/
* https://en.wikipedia.org/wiki/Skip_list
* http://blog.wjin.org/posts/redis-internal-data-structure-skiplist.html
* https://ticki.github.io/blog/skip-lists-done-right/
* https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/SkipList.html
* https://ee.usc.edu/~redekopp/cs104/slides/L23_SkipLists.pdf