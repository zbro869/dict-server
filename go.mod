module gitlab.com/zbro869/dict-server

go 1.14

require (
	github.com/cespare/xxhash v1.1.0
	github.com/ryszard/goskiplist v0.0.0-20150312221310-2dfbae5fcf46 // indirect
	github.com/stretchr/testify v1.6.1
)
